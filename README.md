# avue-plugin-map

## avue 地图插件

## Avue官网
[https://avue.top](https://avue.top)

## npm
[avue-plugin-map](https://www.npmjs.com/package/avue-plugin-map)

## git
[avue-plugin-map](https://gitee.com/smallweigit/avue-plugin-map)

## demo
<p align="center">
  <img width="600" src="https://gitee.com/smallweigit/avue-plugin-map/raw/master/packages/demo/demo.png">
</p>

## use
```
1.安装
npm install avue-plugin-map --save

2.导入
import AvueMap from 'avue-plugin-map'
Vue.use(AvueMap);

3.使用
...
column:[
  ...
    {
      label:'test',
      prop:'test',
      component: "Map"
    }
  ...
]
或者直接
<avue-map v-model="text" ></avue-map>
...
```


